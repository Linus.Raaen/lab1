package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        // TODO: Implement Rock Paper Scissors
        
        int resultat; 
        while (true) {
            int testing = 0;
            System.out.printf("Let's play round %s %n", roundCounter);
            int datamaskinVelger = 1;
            String datamaskinValg = rpsChoices.get(datamaskinVelger);
            String menneskeValg = readInput("Your choice (Rock/Paper/Scissors)?").toLowerCase();
            
            while (testing==0){
            int indeksering = rpsChoices.indexOf(menneskeValg);
            if (indeksering==-1){
                System.out.printf("I do not understand %s. Could you try again? %n", menneskeValg);
                menneskeValg = readInput("Your choice (Rock/Paper/Scissors)?").toLowerCase();

            } else {
                testing = 1;
            
                int utregning = indeksering - datamaskinVelger;
                if (utregning == 0){
                    resultat = 0;
                    System.out.printf("Human chose %s, computer chose %s. It's a tie! %n",menneskeValg,datamaskinValg);
                        } else if (utregning==1 || utregning==-2) {
                            resultat=2;
                            humanScore ++;
                            System.out.printf("Human chose %s, computer chose %s. Human wins! %n",menneskeValg,datamaskinValg);
                        } else if (utregning==-1 || utregning==2) {
                            resultat=1;
                            computerScore ++;
                            System.out.printf("Human chose %s, computer chose %s. Computer wins! %n",menneskeValg,datamaskinValg);
                    }

                    
            }}
            System.out.println("Score: human " + humanScore + ", computer " + computerScore);
                    String fortsettelse = readInput("Do you wish to continue playing? (y/n)?" ).toLowerCase();
                    if (fortsettelse.equals("n")){
                        System.out.print("Bye bye :)");
                        break;
                    }   else if(fortsettelse.equals("y")){
                        roundCounter ++;
                    }
            
                }}
            
    
    

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
